-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
-- Widget library
local vicious = require("vicious")
local batacpi = require("batacpi")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
beautiful.init(os.getenv("HOME") .. "/.config/awesome/theme/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "urxvtcd"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts =
{
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.tile,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.floating,
}
-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag({ 1, 2, 3, 4, 5, 6, 7, 8, 9 }, s, layouts[1])
end
-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu
myawesomemenu = {
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", awesome.quit }
}

--mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
--                                    { "open terminal", terminal }
--                                  }
--                        })

--mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
--                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibox
-- Create a textclock widget
mytextclock = awful.widget.textclock()

-- Helper function: Used to check for batteries below.

-- Battery
-- I could probably combine get_batteries and get_bat_handler and just put the handlers in the batteries table, but... *shrug*
-- This works and is easier to read later
-- If I want to get any fancier than this, though, the batteries table should possible become multidimensional with the widgets, handlers, and any other info I plan to use in it. Maybe state information to show a notification when plugged in/unplugged.
local batteries = {}
local batterywidgets = {}
local bat_handlers = {}
function get_batteries()
   -- Builds the batteries table
   local i = 1
   for bat in awful.util.pread("ls /sys/class/power_supply"):gmatch("[^\r\n]+") do
      if awful.util.file_readable("/sys/class/power_supply/" .. bat .. "/charge_full") then
	 batteries[i] = bat
	 i = i + 1
      end
   end
   return batteries
end
get_batteries()

function get_bat_handler(bat)
   -- Memoized. Returns a function used to format the battery string.
   if not bat_handlers[bat] then
      bat_handlers[bat] = function(widget, args)
	 if args[2] > 15 then
	    bat_10_triggered = false
	    bat_5_triggered = false
	 elseif args[1] ~= "+" then
	    if args[2] < 2 then
	       naughty.notify({ preset = naughty.config.presets.critical,
				title = "PLUG ME IN!",
				text = "PLUG ME IN PLUG ME IN PLUG ME IN!" })
	    elseif args[2] < 5 and not bat_5_triggered then
	       naughty.notify({ preset = naughty.config.presets.critical,
				title = "Going to shut off soon!",
				text = "I hope you have the cable in hand." })
	       bat_5_triggered = true
	    elseif args[2] < 10 and not bat_10_triggered then
	       naughty.notify({ title = "Feeling a little droopy here!",
				text = "Mind plugging me in?" })
	       bat_10_triggered = true
	    end
	 end
	 return(string.format(" [ Bat%s: %d%%-%s%s ] ", bat, args[2], args[3], args[1]))
      end
   end
   return bat_handlers[bat]
end

for i=1,#batteries do
   batterywidgets[i] = wibox.widget.textbox()
   -- The below actually returns a 4th value, the percentage wear and tear on the battery. Use it if you want. I recommend the ✚ glyph in red.
   vicious.register(batterywidgets[i], vicious.widgets.bat, get_bat_handler(i), 31, batteries[i])
end

-- Memory
-- Initialize widget
memwidget = awful.widget.progressbar()
-- Progressbar properties
memwidget:set_width(8)
memwidget:set_height(10)
memwidget:set_vertical(true)
memwidget:set_background_color("#494B4F")
memwidget:set_border_color(nil)
memwidget:set_color({ type = "linear", from = { 0, 0 }, to = { 10,0 }, stops = { {0, "#AECF96"}, {0.5, "#88A175"}, 
                    {1, "#FF5656"}}})
vicious.register(memwidget, vicious.widgets.mem, "$1", 13)

-- Net Traffic
-- Build an array of net adapters and build ourselves our stats string
function build_net_string(netwidget, args)
   active_connections = {}
   for k,v in pairs(vicious.widgets.net(netwidget, args)) do
      if v == 1 then
	 adapter = string.match(k, "(%w+) carrier")
	 if adapter ~= "lo" then -- Don't particularly care about loopback
	    table.insert(active_connections, adapter)
	 end
      end
   end
   local outstring = ""
   for i,v in ipairs(active_connections) do
      if i == 1 then
	 outstring = "[ "
      else
	 outstring = outstring .. " | "
      end
      outstring = outstring .. v .. " ▾" .. args["{" .. v .. " down_kb}"] .. " ▴" .. args["{" .. v .. " up_kb}"]
      if i == #active_connections then
	 outstring = outstring .. " ] "
      end
   end
   return outstring
end

netwidget = wibox.widget.textbox()
vicious.register(netwidget, vicious.widgets.net, build_net_string, 7)


-- Wifi widget
wifiwidget = wibox.widget.textbox()
-- vicious.register(wifiwidget, vicious.widgets.wifi,
--function (wifiwidget, args)

-- Pkg Updates
updatewidget = wibox.widget.textbox()
vicious.register(updatewidget, vicious.widgets.pkg,
		 -- Only show updates if we have any
		 function (updatewidget, args)
		    if args[1] > 0 then
		       return "[ Pkg▴: " .. args[1] .. " ] "
		    else
		       return ""
		    end
		 end, 21599, "Arch C")

-- Weather
weatherwidget = wibox.widget.textbox()
vicious.register(weatherwidget, vicious.widgets.weather,
		 -- I want the "feels like" temperature, so I compute it using the Australian Apparent Temperature formula
		 function (weatherwidget, args)
		    if args["{tempc}"] ~= "N/A" then -- We have results
		       local ws = 0
		       if args["{windmph}"] ~= "N/A" then -- We get the string "N/A" when there is no wind.
			  ws = args["{windmph}"]
		       end
		       local e = args["{humid}"] / 100 * 6.105 * math.exp(17.27 * args["{tempc}"] / (237.7 + args["{tempc}"]))
		       local at = args["{tempc}"] + 0.348 * e - 0.7 * ws
		       local atf = at * 9 / 5 + 32
		       if ws >= 5 and ws < 25 then -- Don't show wind if there isn't any
			  return string.format(" [ %s %sF/%sC - %sF - %smph 연! - %s%% ] ", args["{sky}"], math.ceil(atf), math.ceil(at), args["{tempf}"], ws, args["{humid}"])
		       elseif ws > 0 then
			  return string.format(" [ %s %sF/%sC - %sF - %smph - %s%% ] ", args["{sky}"], math.ceil(atf), math.ceil(at), args["{tempf}"], ws, args["{humid}"])
		       else
			  return string.format(" [ %s %sF/%sC - %sF - %s%% ] ", args["{sky}"], math.ceil(atf), math.ceil(at), args["{tempf}"], ws, args["{humid}"])
		       end
		    else
		       return " [ Look Outside ] "
		    end
		 end,
		 3607, "KSFB")

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
--    left_layout:add(mylauncher)
    left_layout:add(mytaglist[s])
    left_layout:add(mypromptbox[s])

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    right_layout:add(weatherwidget)
    right_layout:add(updatewidget)
    right_layout:add(netwidget)
    right_layout:add(memwidget)
    for i=1,#batterywidgets do
       right_layout:add(batterywidgets[i])
    end
    right_layout:add(mylayoutbox[s])
    right_layout:add(mytextclock)
    if s == 1 then right_layout:add(wibox.widget.systray()) end

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),
    awful.key({ modkey,           }, "x", function () awful.util.spawn("i3lock -c 000000 -d") end),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Multimedia keys
    awful.key({}, "XF86AudioLowerVolume", function () awful.util.spawn("amixer -q sset PCM 2dB-") end),
    awful.key({}, "XF86AudioRaiseVolume", function () awful.util.spawn("amixer -q sset PCM 2dB+") end),
    awful.key({}, "XF86AudioMute", function () awful.util.spawn("amixer -q sset Master toggle") end),
    awful.key({}, "XF86AudioPrev", function () awful.util.spawn("mpc -q prev") end),
    awful.key({}, "XF86AudioNext", function () awful.util.spawn("mpc -q next") end),
    awful.key({}, "XF86AudioStop", function () awful.util.spawn("mpc -q stop") end),
    awful.key({}, "XF86AudioPlay", function () awful.util.spawn("mpc -q toggle") end),
    awful.key({}, "XF86Mail", function () awful.util.spawn("emacsclient -ce '(mu4e)'") end),
    awful.key({}, "XF86HomePage", function () awful.util.spawn("firefox") end),
    awful.key({}, "XF86Calculator", function () awful.util.spawn("xscreensaver-command -lock") end),
--[[ Nothing assigned yet.
    awful.key({}, "XF86WebCam", function () end),
    awful.key({}, "XF86AudioMedia", function () end),
    awful.key({}, "XF86Sleep", function () end),
    awful.key({}, "XF86Tools", function () end), -- Music note key
    awful.key({}, "XF86Search", function () end),
    awful.key({}, "XF86Explorer", function () end),
--]]

    -- Prompt
    awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey }, "y",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Compute the maximum number of digit we need, limited to 9
keynumber = 0
for s = 1, screen.count() do
   keynumber = math.min(9, math.max(#tags[s], keynumber))
end

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, keynumber do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        if tags[screen][i] then
                            awful.tag.viewonly(tags[screen][i])
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

function Set (list)
   local set = {}
   for _, l in ipairs(list) do set[l] = true end
   return set
end

float = Set{"MPlayer", "pinentry", "gimp", "minecraft", "QEMU", "EDiff", }

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "MPlayer" },
      properties = { floating = true } },
    { rule = { class = "mpv" },
      properties = { floating = true } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    { rule = { class = "minecraft" },
      properties = { floating = true } },
    { rule = { class = "QEMU" },
      properties = { floating = true } },
    { rule = { class = "EDiff" },
      properties = { floating = true } },
    -- Set Firefox to always map on tags number 2 of screen 1.
    { rule = { class = "Firefox" },
      properties = { tag = tags[1][2] } },
    -- And Chromium on 2 as well.
    { rule = { class = "Chromium" },
      properties = { tag = tags[1][2] } },
    -- Do the same for gajim, on 9.
    { rule = { class = "Gajim.py" },
      properties = { tag = tags[1][9] } },
    -- Useful for Gajim, if I can work out how.
    --awful.tag.setproperty(tags[s][9], "mwfact", 0.13)
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local title = awful.titlebar.widget.titlewidget(c)
        title:buttons(awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                ))

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(title)

        awful.titlebar(c):set_widget(layout)
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
